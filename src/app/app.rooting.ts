import { Routes, RouterModule } from '@angular/router';
import { HomePage, SandPage } from '../pages/*';

const appRoutes: Routes = [
    { path: 'sand-page', component: SandPage },
    { path: '', component: HomePage },


    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes)
