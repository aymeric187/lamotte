import { BrowserModule  } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';


import { BiblioActifsModule } from '../pages/biblio-actifs/biblio-actifs.module';
import { HomeModule } from '../pages/home/home.module';
import { PdfPageModule } from '../pages/pdf/pdf.module';
import { ContainerPageModule } from '../pages/container/container.module';


import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'cb4c844c'
  }
};



@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings),
    BrowserAnimationsModule,
    HomeModule,
    BiblioActifsModule,
    ContainerPageModule,
    PdfPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
