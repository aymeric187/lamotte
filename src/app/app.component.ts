import {Component, ViewChild} from '@angular/core';
import {Platform, Nav} from 'ionic-angular';
import {Deeplinks} from 'ionic-native';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ContainerPage } from '../pages/container/container';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = ContainerPage;
  @ViewChild(Nav) nav:Nav;


  constructor(private platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  ngAfterViewInit() {
    this.platform.ready().then(() => {
      Deeplinks.routeWithNavController(this.nav, {
        '/homepage': ContainerPage,
      });
    });
  }

}
