import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Platform } from 'ionic-angular';


/**
 * Generated class for the PdfPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-pdf',
  templateUrl: 'pdf.html',
})
export class PdfPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private iab: InAppBrowser, public plt: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PdfPage');
  }

  openPDF(url){
  if(this.plt.is('ios')){
    this.iab.create(url, "_system", "closebuttoncaption=Close");
  }else if(this.plt.is('android')){
    this.iab.create('http://docs.google.com/viewer?url='+url, '_system', 'location=yes');
  }
  //  pdf.executeScript()
    //
  }

}
