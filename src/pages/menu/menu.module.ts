import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuComponent } from './menu';
import{ ActiveDirective } from './active.directive'

@NgModule({
  declarations: [
    MenuComponent,
    ActiveDirective
  ],
  imports: [
    IonicPageModule.forChild(MenuComponent),
  ],
  exports: [
    MenuComponent,
  ]
})
export class MenuModule {}
