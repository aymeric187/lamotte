import { Directive, HostBinding, HostListener } from '@angular/core'

@Directive({
  selector: '[mwActive]'
})


export class ActiveDirective{
  @HostBinding('class.active') active = false;
  @HostListener('click') onClick(){
    this.active = true;
  }
}
