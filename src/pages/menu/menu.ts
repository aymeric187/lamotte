import { ElementRef, Component, Output, EventEmitter,  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the Menu page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
  animations: []
 
})



export class MenuComponent {

  menuState:String = '';
  lignMenuState:String = '';
  ligneMenuActive: number;
  @Output() page = new EventEmitter();

  constructor(public element: ElementRef, public navCtrl: NavController, public navParams: NavParams) {
  }

  ngAfterViewInit() {
      let hammer = new window['Hammer'](this.element.nativeElement);
      hammer.get('pan').set({ direction: window['Hammer'].DIRECTION_ALL });

      hammer.on('panright',ev =>{
        this.menuState = 'checked';
      });

      hammer.on('panleft', ev => {
        this.menuState = '';
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Menu');
  }

  toggleMenu(){
        if(this.menuState == 'checked') this.menuState = '';
        else if(this.menuState == '') this.menuState='checked';
    }

    toggleActive(ligneActive){
      this.ligneMenuActive = ligneActive
      this.page.emit(ligneActive);
    }

}
