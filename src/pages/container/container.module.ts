import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContainerPage } from './container';
import { FooterModule } from '../footer/footer.module'
import { MenuModule } from '../menu/menu.module'

@NgModule({
  declarations: [
    ContainerPage
  ],
  imports: [
    IonicPageModule.forChild(ContainerPage),
    FooterModule,
    MenuModule
  ],
  exports: [
    ContainerPage
  ],
  entryComponents:[]
})
export class ContainerPageModule {}
