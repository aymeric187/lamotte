import {
	Component,
	Input,
	ViewChild,
	ViewContainerRef,
	ComponentFactoryResolver,
	ComponentRef
} from '@angular/core';
import { HomePage } from '../home/home'
import { PdfPage } from '../pdf/pdf'


@Component({
    selector: 'page-container',
    templateUrl: 'container.html',
})

export class ContainerPage {
    @Input() page: string;
    @Input() value: string;

    //Get tag child component will be placed
    @ViewChild('target', { read: ViewContainerRef }) target: ViewContainerRef;
	private componentRef:ComponentRef<any>;

	//Child components
    private pages = {
    	HomePage: HomePage,
			PdfPage: PdfPage
    };

    constructor(private compiler: ComponentFactoryResolver ) {
			this.page = ('HomePage')
		}
		toggleActive(refPage){
			this.page = refPage;
			this.newPage();
		}

    //Pass through value to child component
    renderComponent(){
		if (this.componentRef) this.componentRef.instance.value = this.value;
		}

		//Compile child component
	    ngAfterContentInit() {
	    	let childComponent = this.pages[this.page];
	        //Resolve child component
	        let componentFactory = this.compiler.resolveComponentFactory(childComponent);
	        this.componentRef = this.target.createComponent(componentFactory);
	        this.renderComponent();
	    }

			//Compile child component
		    newPage() {
					this.componentRef.destroy();
		    	this.ngAfterContentInit();
		    }


	    //Pass through value to child component when value changes
		ngOnChanges(changes: Object) {
			this.renderComponent();
		}
}
