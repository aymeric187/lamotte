import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BiblioActifsPage } from './biblio-actifs';
import { FooterModule } from '../footer/footer.module'

@NgModule({
  declarations: [
    BiblioActifsPage,
  ],
  imports: [
    IonicPageModule.forChild(BiblioActifsPage),
    FooterModule,
  ],
  exports: [
    BiblioActifsPage
  ]
})
export class BiblioActifsModule {}
